# Phaser3 - Dude
This was made from following the Phaser3 documentation's "Getting Started" guide.

They provided all assets, and the "bones" of the game.

Custom, personal improvements include:

- Support for touch inputs
	- example: mobile phones, tablets, etc.
- Ablity to "land" from a jump more quickly
- Minor game rebalance
	- example: less "jump height"
